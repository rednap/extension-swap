# nope  
  
swap file extension to nope and back (default to tf for terraform files).  
  
### dependencies  
  
python3  
  
### usage  
  
`$ nope filename.tf` # change filename.tf to filename.nope  
  
`$ nope filename.nope` # change filename.nope to filename.tf  
  
`$ nope anotherfile.nope -x py` # use custom extension flag and change anotherfile.nope to anotherfile.py   
  
### install

`$ pip install extension-swap`   
